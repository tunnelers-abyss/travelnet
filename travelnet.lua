travelnet.register_travelnet_box({
	-- "default" travelnet box in yellow
	nodename = "travelnet:travelnet",
	recipe = travelnet.travelnet_recipe,
	color = "#e0bb2d",
	dye = "dye:magenta"
})
